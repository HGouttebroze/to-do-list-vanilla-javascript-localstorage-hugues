Dans cet exercice, nous allons continuer de voir la manipulation du DOM, et découvrir le localStorage.

## Énoncé

Une structure de base (HTML, CSS, chargement du Javascript) vous est fournie.

Cet exercice va permettre de découvrir la manipulation du localstorage et du JSON !

L'application propose :

- la possibilité d'afficher le formulaire
- L'ajout d'une nouvelle tâche
- L'affichage de la liste de tâches enregistrées dans le localStorage afin d'avoir une persistance des données
- l'affichage du détail d'une tâche en cliquant sur cette dernière
- la possibilité d'éditer la tâche : affiche le formulaire avec les données pré-remplies et à l'enregistrement, cela n'ajoute pas une nouvelle tâche mais modifie bien la tâche déjà existante
- possibilité de supprimer toutes les tâches
- _Bonus : possibilité de supprimer chaque tâche individuellement_

## Astuces

- Utiliser les `console.log` pour valider le bon fonctionnement des différentes étapes du code
- tester la manipulation du localstorage
- vérifier la bonne sérialisation et désérialisation du JSON grâce au console.log

## Illustrations

Affichage du formulaire en cliquant sur le `+` :

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/c36f3fbc-68e6-4177-8b3b-793f4fc6a5da/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/c36f3fbc-68e6-4177-8b3b-793f4fc6a5da/Untitled.png)

Affichage de la liste de tâches :

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/d8a13e37-eb1e-4044-9de4-689b234b6a67/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/d8a13e37-eb1e-4044-9de4-689b234b6a67/Untitled.png)

Affichage du détail d'une tâche en cliquant sur son label :

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/9ca2e63e-6750-4316-9873-76056fefc2f7/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/9ca2e63e-6750-4316-9873-76056fefc2f7/Untitled.png)

Édition d'une tâche dont on a affiché les détails, en cliquant sur "éditer cette tâche" :

![https://s3-us-west-2.amazonaws.com/secure.notion-static.com/830d0a0a-9e74-4de9-b3be-e1ba63903d42/Untitled.png](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/830d0a0a-9e74-4de9-b3be-e1ba63903d42/Untitled.png)

### Liens recommandés

## Méthodologie recommandée

[Méthodologie recommandée](https://www.notion.so/69b8c071b0ab4c42a5ec6fa76f5ecde7) :

Afin de bien aborder l'exercice, voici les étapes que nous vous recommandons de respecter :

1. Gérer l'affichage du formulaire au clic sur le `+`

- affiche le formulaire
- le réinitialise (fait un reset)
- et repasse forcément le `data-mode` en "add" ‣

2. Gérer la soumission du formulaire
1. Affichage de la liste
1. Modification de la fonction d'enregistrement
1. Afficher le détail d'une tâche
1. Edition due la tâche
1. Suppression générale : supprime toutes les tâches du localstorage
1. Bonus : suppresion individuelle → afficher un icône à chaque tâche dans la liste qui offre la possibilité de supprimer une tâche individuellement
