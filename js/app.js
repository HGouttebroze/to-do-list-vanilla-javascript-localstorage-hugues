"use strict"; // Mode strict du JavaScript
// pour mettre 1 alias sur le nom de la fonction qu'on importe
// import { save as saveStorage } from "./storage.js";
// attention, on import pas le fichier entier (on peux avec *) mais les fonction 1 par 1
import { save, load } from "./storage.js";

let form;

const STORAGE = "todolist";

// Fonctions
function showForm() {
  form.dataset.mode = "add";
  form.reset();
  form.classList.remove("hide");
}

function displayTasks() {
  // appeler load et parcourir le tableau, creer 1 element ds notre liste
  let taskList = load(STORAGE); // recupere les taches
  // on veut rajouter un ul
  //const UL = document.createElement("ul");
  document.querySelector("#todo").innerHTML = "<ul></ul>";
  const UL = document.querySelector("#todo ul");
  // on parcour le tablea avec 1 foreach car besoin d'1 index
  taskList.forEach((task, index) => {
    //UL.innerHTML +=
    UL.insertAdjacentHTML(
      "beforeend",
      `<li>
      <a class="task ${task.lvl == 100 ? "barre" : ""}" data-index="${index}">
        ${task.name} - ${task.lvl}%
      </a>
    </li>`
    );
  });
}
/*
x = y affectation de valeur
x == y comparaison de valeur
x === y comparaison de valeur et de type

ex: 
40 == "40" renvoie true, ils ont la même valeur 
40 === "40" renvoie false, même valeur (true) mais type différent(false), comme true && false = false
*/
function showDetails() {
  let index = this.dataset.index;
  let taskList = load(STORAGE);
  let task = taskList[index];
  document.querySelector("#task-details h3").innerHTML =
    (task.lvl == 100 ? '<i class="fas fa-check"></i>&nbsp;' : "") +
    task.name +
    " - " +
    task.lvl +
    "%";
  document.querySelector("#task-details p").textContent = task.desc;
  document.querySelector("#task-details a").dataset.index = index;
  document.querySelector("#task-details").classList.remove("hide");
}

function changeDetails() {
  // on annule l'action par defaut
  //event.preventDefault();
  //console.log(event);

  let index = this.dataset.index;
  let taskList = load(STORAGE);
  let task = taskList[index];

  //console.log(task);

  document.querySelector("#name").value = task.name;
  document.querySelector("#lvl").value = task.lvl;
  document.querySelector("#description").value = task.description;

  form.dataset.mode = "edit";

  form.classList.remove("hide");

  // 1 recuperer les valeur de l'index de la tache affichée
  // on les change:
  // TO CHANGE, voir la méthode array.splice():
  //taskList.splice(task);

  //save(STORAGE, taskList);

  // setItem ecrase ce qu'il y avait déjà, avant d'injecter faut recupérer, faut le push au tableau existant
  // save(STORAGE, taskList);

  // displayTasks();
}

function saveTask(event) {
  // on annule l'action par defaut, event contient les données de l'évenenement
  event.preventDefault(); // j'annule le comportement par défaut (1 form renvoie vers l'action définie ds le HTML (comme 1 page PHP par ex. qui renvoie les valeurs en méthode GET(ds l'URL))),
  console.log(event);

  // array

  let taskList = load(STORAGE);
  console.log(taskList);

  //let name = documentdocument.querySelector("#name").value;
  // or
  const task = {
    lvl: document.querySelector("#lvl").value,
    name: document.querySelector("#name").value,
    description: document.querySelector("#description").value,
  };

  if (form.dataset.mode == "add") {
    taskList.push(task);
  } else {
    let index = document.querySelector("#task-details a").dataset.index;
    taskList[index] = task;
    // or with splice
    //taskList.splice(index, 1, task);
  }

  // setItem ecrase ce qu'il y avait déjà, avant d'injecter faut recupérer, faut le push au tableau existant
  save(STORAGE, taskList);

  form.classList.add("hide");
  document.querySelector("#task-details").classList.add("hide");

  displayTasks();
}

/**
 * Fonction de suppression d'un tâche (<li><button id="clear-todo" title="Effacer les tâches"><i class="fa fa-trash"></i></button>)
 */

function deleteAll() {
  //save(STORAGE, []);
  // or
  localStorage.clear(STORAGE);
  displayTasks();
}

// Code principal
// le code sera chargé... (bien revoir le DOMContentLoaded)
document.addEventListener("DOMContentLoaded", () => {
  displayTasks();
  form = document.querySelector("#task-form");
  document.querySelector("#add-task").addEventListener("click", showForm);

  form.addEventListener("submit", saveTask);

  document
    .querySelector("#task-details a")
    .addEventListener("click", changeDetails);
  console.log("ok");

  document
    .querySelectorAll("#todo .task")
    .forEach((a) => a.addEventListener("click", showDetails));

  document.querySelector(".fa-trash").addEventListener("click", deleteAll);
});

/* affichage du détail d'1 tâche */
// - trouver comment ajouter un gestionnaire d'événement sur ces liens qui sont générés via le JS ‣
// - au clic sur un contact dans la liste, grâce à son index (position dans le tableau)
// enregistrée dans ‣, on va pouvoir récupérer la ligne qui nous intéresse dans la liste et afficher
// les détails de la tâche
// - on stocke l'index de la tâche dans la balise "éditer cette tâche"

/* Edition d'1 tâche */
// - on passe le formulaire en `data-mode="edit"` pour changer le comportement lors de la soumission du formulaire
// - on pré-rempli le formulaire avec les infos de la tâche qu'on souhaite modifier
// - à l'enregistrement, si le formulaire est en `data-mode` "edit" alors on vient remplacer le contenu
// de la ligne correspondant à la tâche par les nouvelles informations récupérées
