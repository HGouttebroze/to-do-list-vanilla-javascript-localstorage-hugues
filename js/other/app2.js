"use strict"; // Mode strict du JavaScript
// pour mettre 1 alias sur le nom de la fonction qu'on importe
// import { save as saveStorage } from "./storage.js";
// attention, on import pas le fichier entier (on peux avec *) mais les fonction 1 par 1
import { save, load } from "./storage.js";

let form;

const STORAGE = "todolist";
// Fonctions
function showForm() {
  form.classList.toggle("hide");
  //console.log("ok");
}

function displayTasks() {
  // appeler load et parcourir le tableau, creer 1 element ds notre liste
  let taskList = load(STORAGE); // recupere les taches
  // on veut rajouter un ul
  //const UL = document.createElement("ul");
  document.querySelector("#todo").innerHTML = "<ul></ul>";
  const UL = document.querySelector("#todo ul");
  // on parcour le tablea avec 1 foreach car besoin d'1 index
  taskList.forEach((task, index) => {
    //UL.innerHTML +=
    UL.insertAdjacentHTML(
      "beforeend",
      `<li>
      <a class="task" data-index="${index}">
        ${task.name} - ${task.lvl}%
      </a>
    </li>`
    );
  });
}

function showDetails() {
  console.log(this);
  let index = this.dataset.index;
  let taskList = load(STORAGE);
  let task = taskList[index];
  document.querySelector("#task-details h3").textContent =
    task.name + " - " + task.lvl + "%";
  document.querySelector("#task-details p").textContent = task.desc;
  document.querySelector("#task-details a").dataset.index = index;
  document.querySelector("#task-details").classList.remove("hide");

  //this.dataset.
  //this.classList.add("href", "#");
  //   let details = document.querySelector("h3");
  //   details.setAttribute("href", this.href);
  //   a.classList.add("task");
  //   details.classList.remove("hide");
}

function changeDetails(event) {
  document
    .querySelector("#task-details a")
    .addEventListener("click", changeDetails);
  console.log("ok");

  //let index = (this.dataset = "edit");
  // on annule l'action par defaut
  event.preventDefault();
  console.log(event);

  //let index = this.dataset.index;
  let taskList = load(STORAGE);
  //let task = taskList[index];

  console.log(taskList);

  const task = {
    lvl: document.querySelector("#lvl").value,
    name: document.querySelector("#name").value,
    description: document.querySelector("#description").value,
  };

  // 1 recuperer les valeur de l'index de la tache affichée
  // on les change:
  // TO CHANGE, voir la méthode array.splice():
  taskList.splice(task);

  // setItem ecrase ce qu'il y avait déjà, avant d'injecter faut recupérer, faut le push au tableau existant
  save(STORAGE, taskList);

  form.classList.remove("hide");
  displayTasks();
}

function saveTask(event) {
  // on annule l'action par defaut
  event.preventDefault();
  console.log(event);

  // array

  let taskList = load(STORAGE);
  console.log(taskList);

  //let name = documentdocument.querySelector("#name").value;
  // or
  const task = {
    lvl: document.querySelector("#lvl").value,
    name: document.querySelector("#name").value,
    description: document.querySelector("#description").value,
  };

  taskList.push(task);

  // setItem ecrase ce qu'il y avait déjà, avant d'injecter faut recupérer, faut le push au tableau existant
  save(STORAGE, taskList);

  form.classList.add("hide");
  displayTasks();
  //this.reset();

  // on récupère le Json
  // taskList = localStorage.getItem("todolist");
  //load(STORAGE, taskList);

  // on le converti
  // mettre setItem & getItem ds des fonction séparé (add, delete...)
  //const task = JSON.parse(localStorage.getItem("todolist"));
  // console.log(JSON.parse(taskList));
  // recup des données, mise à jour des data afin de ne pas les écraser!!!
}

/**
 * Fonction de suppression d'un tâche
 */

// Code principal
// le code sera chargé... (bien revoir le DOMContentLoaded)
document.addEventListener("DOMContentLoaded", () => {
  displayTasks();
  form = document.querySelector("#task-form");
  document.querySelector("#add-task").addEventListener("click", showForm);

  form.addEventListener("submit", saveTask);

  document
    .querySelector("#task-details a")
    .addEventListener("click", changeDetails);
  console.log("ok");
  form.addEventListener("submit", changeDetails);

  document
    .querySelectorAll("#todo .task")
    .forEach((a) => a.addEventListener("click", showDetails));

  //   let details = document.querySelector("#task-details");
  //   details.classList.remove("hide");

  // affichage de la liste des tâches
  // exemple sur 1 array:
  //   const listOfLI = document.querySelectorAll("li"); //cible toutes les li
  //   for (let li of listOfLI) {
  //     li.addEventListener("click", onClickSelect); //installe sur chacune des li ciblées le gestionnaire d'événement
  //   }
  // exemple:
  // sur 1 element qui n'existe pas encore
  //   document
  //     .querySelector("#element-qui-existe")
  //     .addEventListener("click", function (event) {
  //       console.log(event.target); //contient la balise sur laquelle on a trouvé qui se trouve dans "#element-qui-existe"
  //       if (event.target.classList.contains("class-qui-est-sur-notre-element")) {
  //         fonction_a_appeler();
  //       }
  //     });

  //   document
  //     .querySelector("#element-qui-existe")
  //     .addEventListener("click", function (event) {
  //       console.log(event.target); //contient la balise sur laquelle on a trouvé qui se trouve dans "#element-qui-existe"
  //       if (event.target.classList.contains("class-qui-est-sur-notre-element")) {
  //         displayTasks();
  //       }
  //     });
  //1. créer un list de li (createElement), injecter les li au niveu de  <section id="todo"></section> ???
  //2. ciblerenlever la classe hide sur l'affichage d'une tâche
  /* <aside id="task-details" class="hide">
     <h3></h3>
     <p></p>
     <a href="#">Editer cette tâche</a>
   </aside>;
   */
});
