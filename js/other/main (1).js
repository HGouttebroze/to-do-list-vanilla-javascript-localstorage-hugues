"use strict";

import { save, load } from "./storage.js";

/*************************
 * VARIABLES
 ************************/

let form;
const STORAGE = "todolist";

/*************************
 * FONCTIONS
 ************************/

function showForm() {
  form.dataset.mode = "add";
  form.reset();
  form.classList.remove("hide");
}

function displayTasks() {
  let taskList = load(STORAGE); //récupération des taches dans le localstorage

  document.querySelector("#todo").innerHTML = "<ul></ul>";
  const UL = document.querySelector("#todo ul");
  taskList.forEach((task, index) => {
    UL.insertAdjacentHTML(
      "beforeend",
      `<li>
        <a class="task ${
          task.lvl == 100 ? "barre" : ""
        }" data-index="${index}">${task.label} - ${task.lvl}%</a>
      </li>`
    );
  });

  document
    .querySelectorAll("#todo .task")
    .forEach((a) => a.addEventListener("click", showDetail));
}

function saveTask(event) {
  event.preventDefault();
  let taskList = load(STORAGE); //récupération des taches dans le localstorage

  //cibler et récupérer la valeur du nom
  const task = {
    lvl: document.querySelector("#lvl").value,
    label: document.querySelector("#name").value,
    desc: document.querySelector("#description").value,
  };

  if (form.dataset.mode == "add") {
    taskList.push(task);
  } else {
    let index = document.querySelector("#task-details a").dataset.index;
    taskList[index] = task;
    //ou
    taskList.splice(index, 1, task);
  }

  save(STORAGE, taskList);

  form.classList.add("hide");
  document.querySelector("#task-details").classList.add("hide");
  displayTasks();
}

function showDetail() {
  let index = this.dataset.index;
  let taskList = load(STORAGE);
  let task = taskList[index];

  document.querySelector("#task-details h3").innerHTML =
    (task.lvl == 100 ? '<i class="fas fa-check"></i>&nbsp;' : "") +
    task.label +
    " - " +
    task.lvl +
    "%";
  document.querySelector("#task-details p").textContent = task.desc;
  document.querySelector("#task-details a").dataset.index = index;

  document.querySelector("#task-details").classList.remove("hide");
}

function editTask() {
  let index = this.dataset.index;
  let taskList = load(STORAGE);
  let task = taskList[index];

  document.querySelector("#lvl").value = task.lvl;
  document.querySelector("#name").value = task.label;
  document.querySelector("#description").value = task.desc;

  form.dataset.mode = "edit";
  form.classList.remove("hide");
}

function deleteAll() {
  save(STORAGE, []);
  //localStorage.clear(STORAGE);
  displayTasks();
}

/*************************
 * CODE PRINCIPAL
 ************************/

document.addEventListener("DOMContentLoaded", () => {
  displayTasks();

  form = document.querySelector("#task-form");
  document.querySelector("#add-task").addEventListener("click", showForm);
  document.querySelector("#clear-todo").addEventListener("click", deleteAll);
  document.querySelector("#task-details a").addEventListener("click", editTask);

  form.addEventListener("submit", saveTask);
});
