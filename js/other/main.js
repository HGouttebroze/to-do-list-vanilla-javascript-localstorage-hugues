"use strict";

/********************************************************************************************************************************
 * **************************************** STEP CODING ***************************************************************************
 ********************************************************************************************************************************/
// 1. Affichage formulaire + point/event/objet dynamique/localstorage

/********************************************************************************************************************************
 * **************************************** VARIABLES ***************************************************************************
 ********************************************************************************************************************************/

// je déclare 1 variable globale (je ne lui affecterais 1 valeur qu'une faois le HTML bien chargé, donc ds le DOMContentLoaded)
let form;

/********************************************************************************************************************************
 * **************************************** FONCTIONS ***************************************************************************
 ********************************************************************************************************************************/

/**
 * fonction d'affichage du formulaire
 */
function showForm() {
  form.classList.toggle("hide"); // supprimer la classe hide au click pr afficher form
}

/**
 * Fonction d'enregistrement d'une tâche
 */
function saveTask(event) {
  // event contient les données de l'évenenement
  event.preventDefault(); // j'annule le comportement par défaut (1 form renvoie vers l'action définie ds le HTML (comme 1 page PHP par ex. qui renvoie les valeurs en méthode GET(ds l'URL))),
  let taskList = []; // creation d'1 array taskList (il dervra charger les données du localstorage), et on va lui push la tâche (task)

  // je cible et recupere la valeur du nom de la tâche, ds 1 variable
  //let name = document.querySelector("#name").value; // une foi la valeur récupéré, je créé un objet task
  const task = {
    lvl: document.querySelector("#lvl").value,
    name: document.querySelector("#name").value,
    description: document.querySelector("#description").value,
  };

  // je push la tache(task) ds l'array ci-dessus
  taskList.push(task);

  // enregistrement ds le localStorage, où je lui ajoute un item(qui attend 2 param: key & value(l'array qu'on ne va pas push ms transformer en JSON) ,setItem écrase les valeurs))
  // Serialisation du JSon, Recuperation du Json, Ajout ds le localStorage à la clé todolist
  localStorage.setItem("todolist", JSON.stringify(taskList)); // JSON.stringify(donnée à stringifier) (JSON: JavaScripT Object Notation)

  // on utilise getItem pour recupérer la valeur correspondante à la clé todolist
  taskList = localStorage.getItem("todolist"); // retourne le contenu de la value de la key "todolist, à stocker ds 1 variable "taskList"
  console.log(taskList); // je recupère je Json

  // depuis le Json, il faut retourner 1 objet pour retrouver un array,
  //on recupere le Json pr le transformer en objet complexe qu'on pourra alors manipuler
  JSON.parse(taskList);
}
/********************************************************************************************************************************
 * **************************************** CODE PRINCIPAL ***************************************************************************
 ********************************************************************************************************************************/
// creation d'1 fonction anonyne qui sera appelé qd la page HTML sera bien chargée, donc 1 eventListener sur le HTML
document.addEventListener("DOMContentLoaded", () => {
  form = document.querySelector("#task-form");
  document.querySelector("#add-task").addEventListener("click", showForm); // à n'installer qu'une fois la page HTML chargée

  form.addEventListener("submit", saveTask); // au submit, appel de la fonction saveTask
});

// TODO:
// c'est donc là qu'il faut changer la t^che et non pas la push sur le détail d'1 tache
//let taskList = []; // creation d'1 array taskList (il dervra charger les données du localstorage), et on va lui push la tâche

// Rappel:
// "taskList" est une variable contenant la value de la key "todolist",
// "task" est 1 objet contenant les valeur des taches (le nom, le level & la description), et on push cet obet ds l' array
// enregistrement ds le localStorage, où je lui ajoute un item(qui attend 2 param: key & value(l'array qu'on ne va pas push ms transformer en JSON) ,setItem écrase les valeurs))
// // JSON.stringify(donnée à stringifier) (JSON: JavaScripT Object Notation)
// getItem permet de retourner le contenu de la value de la key "todolist, à stocker ds 1 variable "taskList"
