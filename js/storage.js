"use strict";

/**
 * Enregistre un tableau en JSON ds le localStorage
 * @param {string} key Nom de la clé correspondante à l'item ds le localStorage
 * @param {Array} list Tableau de données à stocker
 */
export function save(key, list) {
  localStorage.setItem(key, JSON.stringify(list));
}

/**
 *
 * @param {string} key
 * @returns {Array}
 */
export function load(key) {
  const datas = localStorage.getItem(key);
  return datas != null ? JSON.parse(datas) : [];
}
